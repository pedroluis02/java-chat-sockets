/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/**
 *
 * @author Pedro Luis
 */

import datos.Usuario;
import datos.Mensaje;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class HiloCliente extends Thread {
    private VentanaCliente ventanaCliente;
    private DataInputStream flujoLectura;
    private DataOutputStream flujoEscritura;
    public Usuario usuario;
    
    public HiloCliente(Socket socket, VentanaCliente ventanaCliente, String nombre){
        this.ventanaCliente = ventanaCliente;
        this.usuario = new Usuario();
        usuario.nombre = nombre;
        try{
            flujoLectura = new DataInputStream(socket.getInputStream());
            flujoEscritura = new DataOutputStream(socket.getOutputStream());
        }
        catch(IOException ie){
            ventanaCliente.mensajeErrorBarra(ie.getMessage());
        }
    }
    @Override
    public void run() {
        int tipo;
        try {
            while(true) {
                tipo = flujoLectura.readInt();
                switch(tipo) {
                    case Mensaje.REGISTRO:
                        usuario.id = flujoLectura.readInt();
                        ventanaCliente.mensajeBarra("Id: " + usuario.id);
                        enviarDatos();
                        break;
                    case Mensaje.LISTA_USUARIOS:
                        //
                        ventanaCliente.setDatosUsuario(usuario);
                        //
                        int nu = flujoLectura.readInt();
                        if(nu == 0) {
                            ventanaCliente.mensajeBarra("No hay usuarios conectados.");
                            ventanaCliente.cambioGUIConectado();
                            break;
                        }
                        ventanaCliente.mensajeBarra("Recibiendo lista de usuarios...");
                        int i=0, e_,  i_; String n_, m_;
                        while(i < nu) {
                            i_ = flujoLectura.readInt();
                            n_ = flujoLectura.readUTF();
                            m_ = flujoLectura.readUTF();
                            e_ = flujoLectura.readInt();
                            ventanaCliente.addUsuarioLista(new Usuario(
                                    i_, n_, m_, e_));
                            i++;
                        }
                        ventanaCliente.mensajeBarra("Lista de usuarios recibida.");
                        //
                        ventanaCliente.cambioGUIConectado();
                        break;
                    case Mensaje.USUARIO_CONECTADO:
                        int ni = flujoLectura.readInt(); 
                        String nn = flujoLectura.readUTF(), 
                               nm = flujoLectura.readUTF();
                        int ne = flujoLectura.readInt();
                        
                        ventanaCliente.addUsuarioLista(new Usuario(ni, nn, nm, ne));
                        ventanaCliente.mensajeBarra(
                                "Se ha Conectado: id=" + ni + " nombre=" + nn);
                        break;
                        
                    case Mensaje.CAMBIO_NOMBRE:
                        int c_ni = flujoLectura.readInt();
                        String n_nombre = flujoLectura.readUTF();
                        ventanaCliente.cambioNombreUsuario(c_ni, n_nombre);
                        break;
                    case Mensaje.CAMBIO_MENSAJE_PERSONAL:
                        int c_mi = flujoLectura.readInt();
                        String n_mensaje = flujoLectura.readUTF();
                        ventanaCliente.cambioMensajePersonalUsuario(c_mi, n_mensaje);
                        break;
                    case Mensaje.CAMBIO_ESTADO:
                        int c_ne = flujoLectura.readInt(),
                            n_e = flujoLectura.readInt();
                        ventanaCliente.cambioEstadoUsuario(c_ne, n_e);
                        break;
                    case Mensaje.USUARIO_DESCONECTADO:
                        int ir = flujoLectura.readInt();
                        ventanaCliente.removeUsuarioLista(ir);
                        break;
                    case Mensaje.MENSAJE_GNERAL:
                        int id_contacto = flujoLectura.readInt();
                        String mensaje_contacto = flujoLectura.readUTF();
                        //
                        ventanaCliente.mostrarMensajeHiloGeneral(id_contacto, mensaje_contacto);
                        //
                        break;
                    case Mensaje.MENSAJE_PRIVADO:
                        int idUsuario = flujoLectura.readInt(),
                            idContacto = flujoLectura.readInt();
                        String mensajeContacto = flujoLectura.readUTF();
                        //
                        ventanaCliente.mostrarMensajeHiloPrivado(idUsuario, 
                                idContacto, mensajeContacto);
                        //
                        break;
                }
            }
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
        }
        ventanaCliente.cambioGuiDesconectado();
    }
    //
    public void enviarDatos() {
        try {
            flujoEscritura.writeInt(Mensaje.DATOS);
            flujoEscritura.writeUTF(usuario.nombre);
            flujoEscritura.writeUTF(usuario.mensajePersonal);
            flujoEscritura.writeInt(usuario.estado);
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
        }
    }
    //
    public void enviarCambioNombre(String nuevoNombre) {
        if(usuario.nombre.compareTo(nuevoNombre) == 0) {
            return;
        }
        
        usuario.nombre = nuevoNombre;
        
        try {
            flujoEscritura.writeInt(Mensaje.CAMBIO_NOMBRE);
            flujoEscritura.writeUTF(usuario.nombre);
            //
            usuario.nombre = nuevoNombre;
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
        }
    }
    public void enviarCambioMensajePersonal(String nuevoMensajePersonal) {
        if(usuario.mensajePersonal.compareTo(nuevoMensajePersonal) == 0) {
            return;
        }
        
        usuario.mensajePersonal = nuevoMensajePersonal;
        
        try {
            flujoEscritura.writeInt(Mensaje.CAMBIO_MENSAJE_PERSONAL);
            flujoEscritura.writeUTF(usuario.mensajePersonal);
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
        }
    }
    public void enviarCambioEstado(int nuevoEstado) {        
        usuario.estado = nuevoEstado;
        
        try {
            flujoEscritura.writeInt(Mensaje.CAMBIO_ESTADO);
            flujoEscritura.writeInt(usuario.estado);
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
        }
    }
    //
    public void enviarMensajeGeneral(String mensaje) {
        try {
            flujoEscritura.writeInt(Mensaje.MENSAJE_GNERAL);
            flujoEscritura.writeUTF(mensaje);
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
        }
    }
    public void enviarMensajePrivado(int idContacto, String mensaje) {
        try {
            flujoEscritura.writeInt(Mensaje.MENSAJE_PRIVADO);
            flujoEscritura.writeInt(idContacto);
            flujoEscritura.writeUTF(mensaje);
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
        }
    }
}