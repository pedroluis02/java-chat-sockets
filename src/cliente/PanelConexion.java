
package cliente;

import java.net.InetAddress;

/**
 *
 * @author pedroluis
 */
public class PanelConexion extends javax.swing.JPanel {

    public PanelConexion(VentanaCliente ventanaCliente) {
        initComponents();
        this.ventanaCliente = ventanaCliente;
        try {
            host = InetAddress.getLocalHost().getHostAddress();
            nombre = InetAddress.getLocalHost().getHostName();
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
            host = "localhost";
            nombre = "";
        }
        
        textoNombre.setText(nombre);
        textoIP.setText(host);
        textoPuerto.setText(puerto + "");
        //
        barraConexion.setVisible(false);
    }
//
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        textoIP = new org.edisoncor.gui.textField.TextFieldRoundBackground();
        textoPuerto = new org.edisoncor.gui.textField.TextFieldRoundBackground();
        botonConexion = new org.edisoncor.gui.button.ButtonAqua();
        textoNombre = new org.edisoncor.gui.textField.TextFieldRoundBackground();
        barraConexion = new javax.swing.JProgressBar();

        setMaximumSize(new java.awt.Dimension(334, 485));
        setMinimumSize(new java.awt.Dimension(334, 485));
        setPreferredSize(new java.awt.Dimension(334, 485));

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/logo.png"))); // NOI18N

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 126, Short.MAX_VALUE)
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 126, Short.MAX_VALUE)
        );

        textoIP.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        textoIP.setToolTipText("Dirección IP de Servidor");
        textoIP.setDescripcion("Dir IP de Servidor");

        textoPuerto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        textoPuerto.setToolTipText("Puerto del Servidor");
        textoPuerto.setDescripcion("Puerto del Servidor");

        botonConexion.setMnemonic('c');
        botonConexion.setText("conectar");
        botonConexion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConexionActionPerformed(evt);
            }
        });

        textoNombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        textoNombre.setToolTipText("Nombre de Usuario");
        textoNombre.setDescripcion("Nombre de Usuario");

        barraConexion.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(botonConexion, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                            .addComponent(textoPuerto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(textoIP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(textoNombre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                            .addComponent(barraConexion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(textoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textoIP, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(textoPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(barraConexion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(botonConexion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void botonConexionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConexionActionPerformed
        // TODO add your handling code here:
        host = textoIP.getText();
        try {
            puerto = Integer.parseInt(textoPuerto.getText());
        } catch(Exception ex) {
            ventanaCliente.mensajeErrorBarra(ex.getMessage());
            return;
        }
        //
        cambioGuiConexion(false);
        //
        barraConexion.setVisible(true);
        barraConexion.setIndeterminate(true);
        barraConexion.setString("Conectando...");
        barraConexion.setStringPainted(true);
        //
        nombre = textoNombre.getText();
        new Thread() {

            @Override
            public void run() {
                ventanaCliente.conectar(host, puerto, nombre);
                barraConexion.setVisible(false);
                barraConexion.setIndeterminate(false);
                cambioGuiConexion(true);
            }
            
        }.start();
    }//GEN-LAST:event_botonConexionActionPerformed

    private void cambioGuiConexion(boolean value) {
        
        textoNombre.setEditable(value);
        textoIP.setEditable(value);
        textoPuerto.setEditable(value);
        //
        botonConexion.setEnabled(value);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barraConexion;
    private org.edisoncor.gui.button.ButtonAqua botonConexion;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private org.edisoncor.gui.textField.TextFieldRoundBackground textoIP;
    private org.edisoncor.gui.textField.TextFieldRoundBackground textoNombre;
    private org.edisoncor.gui.textField.TextFieldRoundBackground textoPuerto;
    // End of variables declaration//GEN-END:variables
    private VentanaCliente ventanaCliente;
    private String host;
    private String nombre;
    private int puerto = 5000;
}
