
package cliente;

import datos.Usuario;
import java.awt.Color;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public class PanelCellRenderLista <E> extends JPanel implements ListCellRenderer <E> {
    public PanelCellRenderLista() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelTask = new org.edisoncor.gui.label.LabelTask();
        iconoEstado = new javax.swing.JLabel();

        labelTask.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/user.png"))); // NOI18N
        labelTask.setCategoryFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        labelTask.setCategorySmallFont(new java.awt.Font("Arial", 2, 12)); // NOI18N

        iconoEstado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        iconoEstado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/estado/online.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(labelTask, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(iconoEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(iconoEstado, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                    .addComponent(labelTask, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JLabel iconoEstado;
    protected org.edisoncor.gui.label.LabelTask labelTask;
    // End of variables declaration//GEN-END:variables

    @Override
    public Component getListCellRendererComponent(JList<? extends E> jlist, E value, 
    int index, boolean isSelected, boolean cellHasFocus) {
        
        if(isSelected) {
            setBorder(BorderFactory.createLineBorder(new Color(0, 0, 64)));
            setBackground(jlist.getSelectionBackground());
        } else if(cellHasFocus) {
            setBackground(jlist.getSelectionForeground());
        } else {
            setBorder(null);
            if(index%2 == 0) {
                setBackground(new Color(230, 230, 255));
            } else {
                setBackground(Color.WHITE);
            }
        }
        
        return this;
    } 
    
    public static class Rendirazado_Contactos extends PanelCellRenderLista <Usuario> {

        @Override
        public Component getListCellRendererComponent(JList<? extends Usuario> jlist, 
        Usuario value, int index, boolean isSelected, boolean cellHasFocus) {
            //
            labelTask.setText("(" + value.id + "): " + value.nombre);
            labelTask.setDescription(value.mensajePersonal + " ");
            iconoEstado.setIcon(VentanaCliente.obtenerRecursoIconoEstado(value.estado));
            //
            return super.getListCellRendererComponent(jlist, value, index, isSelected, cellHasFocus); 
        }
    }
}
