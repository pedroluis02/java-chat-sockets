
package cliente;

import datos.Mensaje;
import datos.Usuario;
import chat.VentanaChat;
import java.awt.Color;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.net.Socket;
import java.util.HashMap;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.Timer;

/**
 *
 * @author pedroluis
 */
public class VentanaCliente extends JFrame implements ActionListener {

    public VentanaCliente() {
        initComponents();       
        panelConexion = new PanelConexion(this);
        //
        layoutAux = panelCentro.getLayout();
        //
        panelCentro.setLayout(new BoxLayout(panelCentro, BoxLayout.Y_AXIS));
        panelCentro.add(panelConexion);
        jScrollPane1.setVisible(false);
        
        modeloLista = new DefaultListModel<>();
        listaUsuarios = new HashMap<>();
        
        listaContactos.setModel(modeloLista);
        listaContactos.setCellRenderer(new PanelCellRenderLista.Rendirazado_Contactos());
      
        tiempoMensajeBarra = new Timer(5000, this);
        tiempoMensajeBarra.setActionCommand("tm");
        //
        textoNombre.setVisible(false);
        textoMensajePersonal.setVisible(false);
        //
        panelUsuario.setVisible(false);
        //
        //
        barraMenus.setVisible(false);
        //
        initIconosEstado();
    }

    private void initIconosEstado() {
        JPopupMenu pm = botonMenu_Estado.getPopupMenu();
        miEstados = new JMenuItem[4];
        //
        miEstados[0] = new JMenuItem("Disponible",
                obtenerRecursoIconoEstado(Mensaje.Estado.DISPONIBLE));
        miEstados[1] = new JMenuItem("Ocupado",
                obtenerRecursoIconoEstado(Mensaje.Estado.OCUPADO));
        miEstados[2] = new JMenuItem("Ausente",
                obtenerRecursoIconoEstado(Mensaje.Estado.AUSENTE));
        miEstados[3]= new JMenuItem("Inactivo",
                obtenerRecursoIconoEstado(Mensaje.Estado.INACTIVO));
        //
        int i = 0;
        for(JMenuItem jmi : miEstados) {
            jmi.setActionCommand(i + "");
            jmi.addActionListener(this);
            i++;
        }
        //
        pm.add(miEstados[0]); pm.add(miEstados[1]);
        pm.add(miEstados[2]); pm.add(miEstados[3]);
    }
    // static methods
    private static ImageIcon obtenerRecursoIcono(String nombre) {
        return new ImageIcon(VentanaCliente.class.getResource(RUTA + nombre));
    }
    public static ImageIcon obtenerRecursoIconoEstado(int estado) {
        switch(estado) {
            case Mensaje.Estado.DISPONIBLE: return obtenerRecursoIcono("online.png");
            case Mensaje.Estado.OCUPADO: return obtenerRecursoIcono("busy.png");
            case Mensaje.Estado.AUSENTE: return obtenerRecursoIcono("away.png");
            case Mensaje.Estado.INACTIVO: return obtenerRecursoIcono("idle.png");
            default: return obtenerRecursoIcono("offline.png");
        }
    }
    //
    private void iniciarVentanaChat() {
        if(ventanaChat == null) {
            ventanaChat = new VentanaChat(this, hiloCliente.usuario.id);
            ventanaChat.setLocationRelativeTo(this);
        }
    }
    //
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelCentro = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaContactos = new javax.swing.JList();
        panelUsuario = new javax.swing.JPanel();
        panelAvatar = new org.edisoncor.gui.panel.PanelImage();
        panelDatos = new javax.swing.JPanel();
        botonNombre = new javax.swing.JToggleButton();
        textoNombre = new javax.swing.JTextField();
        botonMensajePersonal = new javax.swing.JToggleButton();
        textoMensajePersonal = new javax.swing.JTextField();
        panelOtros = new javax.swing.JPanel();
        jToggleButton1 = new javax.swing.JToggleButton();
        botonMenu_Estado = new org.edisoncor.gui.button.ButtonPopup();
        jToolBar2 = new javax.swing.JToolBar();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        textoMensaje = new javax.swing.JLabel();
        barraMenus = new javax.swing.JMenuBar();
        menuAplicacion = new javax.swing.JMenu();
        miDesconectar = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        miSalir = new javax.swing.JMenuItem();
        menuChat = new javax.swing.JMenu();
        miChat = new javax.swing.JMenuItem();
        menuOpciones = new javax.swing.JMenu();
        miPreferencias = new javax.swing.JMenuItem();
        menuAyuda = new javax.swing.JMenu();
        miAcercaDe = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        listaContactos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listaContactos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaContactosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(listaContactos);

        panelAvatar.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        panelAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/user.png"))); // NOI18N

        javax.swing.GroupLayout panelAvatarLayout = new javax.swing.GroupLayout(panelAvatar);
        panelAvatar.setLayout(panelAvatarLayout);
        panelAvatarLayout.setHorizontalGroup(
            panelAvatarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 54, Short.MAX_VALUE)
        );
        panelAvatarLayout.setVerticalGroup(
            panelAvatarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        botonNombre.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        botonNombre.setText("Nombre");
        botonNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNombreActionPerformed(evt);
            }
        });

        textoNombre.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        textoNombre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                textoNombreFocusLost(evt);
            }
        });

        botonMensajePersonal.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        botonMensajePersonal.setSelected(true);
        botonMensajePersonal.setText("Mensaje Personal");
        botonMensajePersonal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        botonMensajePersonal.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        botonMensajePersonal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonMensajePersonalActionPerformed(evt);
            }
        });

        textoMensajePersonal.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        textoMensajePersonal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                textoMensajePersonalFocusLost(evt);
            }
        });

        javax.swing.GroupLayout panelDatosLayout = new javax.swing.GroupLayout(panelDatos);
        panelDatos.setLayout(panelDatosLayout);
        panelDatosLayout.setHorizontalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(botonNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(textoNombre)
            .addComponent(botonMensajePersonal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(textoMensajePersonal)
        );
        panelDatosLayout.setVerticalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(botonNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(textoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(botonMensajePersonal, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(textoMensajePersonal, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        );

        jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/search.png"))); // NOI18N

        botonMenu_Estado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/estado/online.png"))); // NOI18N

        javax.swing.GroupLayout panelOtrosLayout = new javax.swing.GroupLayout(panelOtros);
        panelOtros.setLayout(panelOtrosLayout);
        panelOtrosLayout.setHorizontalGroup(
            panelOtrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOtrosLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(panelOtrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonMenu_Estado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jToggleButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        panelOtrosLayout.setVerticalGroup(
            panelOtrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOtrosLayout.createSequentialGroup()
                .addComponent(jToggleButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(botonMenu_Estado, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3))
        );

        javax.swing.GroupLayout panelUsuarioLayout = new javax.swing.GroupLayout(panelUsuario);
        panelUsuario.setLayout(panelUsuarioLayout);
        panelUsuarioLayout.setHorizontalGroup(
            panelUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUsuarioLayout.createSequentialGroup()
                .addComponent(panelAvatar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelOtros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        panelUsuarioLayout.setVerticalGroup(
            panelUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelUsuarioLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(panelUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelOtros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelUsuarioLayout.createSequentialGroup()
                        .addGroup(panelUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(panelDatos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelAvatar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout panelCentroLayout = new javax.swing.GroupLayout(panelCentro);
        panelCentro.setLayout(panelCentroLayout);
        panelCentroLayout.setHorizontalGroup(
            panelCentroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
        );
        panelCentroLayout.setVerticalGroup(
            panelCentroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCentroLayout.createSequentialGroup()
                .addComponent(panelUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE))
        );

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);
        jToolBar2.add(jSeparator1);

        textoMensaje.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jToolBar2.add(textoMensaje);

        menuAplicacion.setMnemonic('A');
        menuAplicacion.setText("Aplicación");

        miDesconectar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        miDesconectar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/menus/disconnect.png"))); // NOI18N
        miDesconectar.setText("Desconectar");
        miDesconectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miDesconectarActionPerformed(evt);
            }
        });
        menuAplicacion.add(miDesconectar);
        menuAplicacion.add(jSeparator2);

        miSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        miSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/menus/quit.png"))); // NOI18N
        miSalir.setText("Salir");
        miSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSalirActionPerformed(evt);
            }
        });
        menuAplicacion.add(miSalir);

        barraMenus.add(menuAplicacion);

        menuChat.setMnemonic('h');
        menuChat.setText("Chat");

        miChat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/menus/chat.png"))); // NOI18N
        miChat.setText("Mostrar ventana...");
        miChat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miChatActionPerformed(evt);
            }
        });
        menuChat.add(miChat);

        barraMenus.add(menuChat);

        menuOpciones.setMnemonic('O');
        menuOpciones.setText("Opciones");

        miPreferencias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/menus/preferences.png"))); // NOI18N
        miPreferencias.setText("Preferencias...");
        menuOpciones.add(miPreferencias);

        barraMenus.add(menuOpciones);

        menuAyuda.setMnemonic('u');
        menuAyuda.setText("Ayuda");

        miAcercaDe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/menus/help.png"))); // NOI18N
        miAcercaDe.setText("Acerca de ...");
        menuAyuda.add(miAcercaDe);

        barraMenus.add(menuAyuda);

        setJMenuBar(barraMenus);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelCentro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(panelCentro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void textoNombreFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textoNombreFocusLost
        // TODO add your handling code here:
        botonNombre.setVisible(true);
        textoNombre.setVisible(false);
        //
        if(textoNombre.getText().isEmpty()) {
            textoNombre.setText(botonNombre.getText());
        } else {
            botonNombre.setText(textoNombre.getText());
            // enviar actualizacion de nombre
            hiloCliente.enviarCambioNombre(textoNombre.getText());
        }
    }//GEN-LAST:event_textoNombreFocusLost

    private void textoMensajePersonalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textoMensajePersonalFocusLost
        // TODO add your handling code here:
        botonMensajePersonal.setVisible(true);
        textoMensajePersonal.setVisible(false);
        //
        if(textoMensajePersonal.getText().isEmpty()) {
            textoMensajePersonal.setText(botonMensajePersonal.getText());
        } else {
            botonMensajePersonal.setText(textoMensajePersonal.getText());
            // enviar actualizacion de mensaje personal
            hiloCliente.enviarCambioMensajePersonal(textoMensajePersonal.getText());
        }
    }//GEN-LAST:event_textoMensajePersonalFocusLost

    private void botonMensajePersonalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonMensajePersonalActionPerformed
        // TODO add your handling code here:
        botonMensajePersonal.setVisible(false);
        textoMensajePersonal.setVisible(true);
        textoMensajePersonal.requestFocus();
        //
        botonMensajePersonal.setSelected(true);
    }//GEN-LAST:event_botonMensajePersonalActionPerformed

    private void botonNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNombreActionPerformed
        // TODO add your handling code here:
        botonNombre.setVisible(false);
        textoNombre.setVisible(true);
        textoNombre.requestFocus();
        //
        botonNombre.setSelected(false);
    }//GEN-LAST:event_botonNombreActionPerformed

    private void miDesconectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miDesconectarActionPerformed
        // TODO add your handling code here:
        String m = desconectar();
        if(!m.isEmpty()) {
            mensajeErrorBarra(m);
        }
        //
        if(ventanaChat != null) {
            ventanaChat.removeAll();
            ventanaChat.dispose();
            ventanaChat = null;
        }
    }//GEN-LAST:event_miDesconectarActionPerformed

    private void miChatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miChatActionPerformed
        // TODO add your handling code here:
        iniciarVentanaChat();
        ventanaChat.setVisible(true);
    }//GEN-LAST:event_miChatActionPerformed

    private void miSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_miSalirActionPerformed

    private void listaContactosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaContactosMouseClicked
        // TODO add your handling code here:
        if(evt.getButton() == MouseEvent.BUTTON1) {
            if(evt.getClickCount() == 2) {
                int index = listaContactos.locationToIndex(evt.getPoint());
                if(index != -1) {
                    iniciarVentanaChat();
                    ventanaChat.nuevoPanelChatPrivado(modeloLista.get(index));
                    //
                    ventanaChat.ponerFocoNuevoPanelChat(modeloLista.get(index).id);
                    //
                    ventanaChat.setVisible(true);
                    //
                    ventanaChat.requestFocus();
                }
            }
        }
    }//GEN-LAST:event_listaContactosMouseClicked

    //
    public void conectar(String host, int puerto, String nombre) {  
        mensajeBarra("Conectando...");
        try {
            socket = new Socket(host, puerto);
            hiloCliente = new HiloCliente(socket, this, nombre);
            hiloCliente.start(); 
        } catch(Exception ex) {
            mensajeErrorBarra(ex.getMessage());
        }        
        //
    }
    private String desconectar() {
        try {
            //hiloCliente.stop();
            hiloCliente.interrupt();
            socket.close();
            
            cambioGuiDesconectado();
            
        } catch(Exception ex) {
            return ex.getMessage();
        }
        return "";
    }
    public void cambioGUIConectado() {
        //
        barraMenus.setVisible(true);
        //
        panelConexion.setVisible(false);
        
        panelUsuario.setVisible(true);
        jScrollPane1.setVisible(true);
        //
        //
        panelCentro.setLayout(layoutAux);
    } 
    public void cambioGuiDesconectado() {
        //
        barraMenus.setVisible(false);
        //
        panelConexion.setVisible(true);
        jScrollPane1.setVisible(false);
        panelUsuario.setVisible(false);
        //
        modeloLista.clear();
        listaUsuarios.clear();
        //
        panelCentro.setLayout(new BoxLayout(panelCentro, BoxLayout.Y_AXIS));
        //
    }
    public void setDatosUsuario(Usuario u) {
        botonNombre.setText(u.nombre);
        textoNombre.setText(u.nombre);
        //
        botonMensajePersonal.setText(u.mensajePersonal.isEmpty() 
                ? "Mensaje Personal..." : u.mensajePersonal);
        textoMensajePersonal.setText(u.mensajePersonal);
    }
    // Mensajes en barra
    public void mensajeBarra(String mensaje) {
        textoMensaje.setForeground(Color.BLACK);
        
        tiempoMensajeBarra.stop();
        textoMensaje.setText("");
        tiempoMensajeBarra.start();
        textoMensaje.setText(mensaje);
        
        System.out.println(mensaje);
    }
    public void mensajeErrorBarra(String mensaje) {
        textoMensaje.setForeground(Color.red);
        //
        tiempoMensajeBarra.stop();
        textoMensaje.setText("");
        tiempoMensajeBarra.start();
        textoMensaje.setText(mensaje);
        //
        System.err.println(mensaje);
    }
    //
    public void addUsuarioLista(Usuario usuario) {
        listaUsuarios.put(usuario.id, usuario);
        modeloLista.addElement(usuario);
    }
    public void removeUsuarioLista(int id) {
        mensajeBarra("Se ha Desconectado: id=" + id);
        //
        Object object = 
                listaUsuarios.remove(id);
        modeloLista.removeElement(object);
        //
        if(ventanaChat != null) {
            ventanaChat.desconexionContacto(id);
        }
    }
    public void cambioNombreUsuario(int id, String nuevoNombre) {
        Usuario u = listaUsuarios.get(id);
        if(u == null) {
            return;
        }
        //
        u.nombre = nuevoNombre;
        //
        mensajeBarra("Cambio Nombre: id=" + id);
        //
        if(ventanaChat != null) {
            ventanaChat.cambioTabTitulo(id, nuevoNombre);
        }
    }
    public void cambioMensajePersonalUsuario(int id, String nuevoMensajePersonal) {
        Usuario u = listaUsuarios.get(id);
        if(u == null) {
            return;
        }
        //
        u.mensajePersonal = nuevoMensajePersonal;
        //
        mensajeBarra("Cambio Mensaje Personal: id=" + id);
    }
    public void cambioEstadoUsuario(int id, int nuevoEstado) {
        Usuario u = listaUsuarios.get(id);
        if(u == null) {
            return;
        }
        //
        u.estado = nuevoEstado;
        //
        mensajeBarra("Cambio Estado: id=" + id);
        //
        if(ventanaChat != null) {
            ventanaChat.cambioTabIcono(id, nuevoEstado);
        }
    }
    // chat - ventana - hilo
    public void enviarMensajeHiloGeneral(String mensaje) {
        hiloCliente.enviarMensajeGeneral(mensaje);
    }
    public void enviarMensajeHiloPrivado(int idContacto, String mensaje) {
        hiloCliente.enviarMensajePrivado(idContacto, mensaje);
    }
    // hilo - ventana - chat
    public void mostrarMensajeHiloGeneral(int id_contacto, String mensaje) {
        iniciarVentanaChat();
        
        if(hiloCliente.usuario.id == id_contacto) {
            ventanaChat.mostraMensajeChatGeneral(hiloCliente.usuario, 
                    hiloCliente.usuario.id, mensaje);
        } else {
            ventanaChat.mostraMensajeChatGeneral(listaUsuarios.get(id_contacto), 
                    hiloCliente.usuario.id, mensaje);
        }
        //
        if(!ventanaChat.isVisible()) {
            ventanaChat.setVisible(true);
        }
    }
    public void mostrarMensajeHiloPrivado(int idUsuario, int idContacto, String mensaje) {
        iniciarVentanaChat();
        //
        if(hiloCliente.usuario.id == idUsuario) {
            ventanaChat.nuevoPanelChatPrivado(listaUsuarios.get(idContacto));
            //
            ventanaChat.mostraMensajeChatPrivado(hiloCliente.usuario, 
                    idContacto, mensaje);
        } else {
            ventanaChat.nuevoPanelChatPrivado(listaUsuarios.get(idUsuario));
            //
            ventanaChat.mostraMensajeChatPrivado(listaUsuarios.get(idUsuario), 
                    idUsuario, mensaje);
            //
            ventanaChat.notificacionTabBarIcono(idUsuario);
        }
        //
        if(!ventanaChat.isVisible()) {
            ventanaChat.setVisible(true);
        }
    }
    //
    @Override
    public void actionPerformed(ActionEvent ae) {
        String a = ae.getActionCommand();
        if(a.compareTo("tm") == 0) {
            textoMensaje.setText("");
            tiempoMensajeBarra.stop();
            return;
        }
        
        int e = Integer.parseInt(a); 
        if(e != hiloCliente.usuario.estado) {
            botonMenu_Estado.setIcon(miEstados[e].getIcon());
            hiloCliente.enviarCambioEstado(e);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barraMenus;
    private javax.swing.JToggleButton botonMensajePersonal;
    private org.edisoncor.gui.button.ButtonPopup botonMenu_Estado;
    private javax.swing.JToggleButton botonNombre;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JList listaContactos;
    private javax.swing.JMenu menuAplicacion;
    private javax.swing.JMenu menuAyuda;
    private javax.swing.JMenu menuChat;
    private javax.swing.JMenu menuOpciones;
    private javax.swing.JMenuItem miAcercaDe;
    private javax.swing.JMenuItem miChat;
    private javax.swing.JMenuItem miDesconectar;
    private javax.swing.JMenuItem miPreferencias;
    private javax.swing.JMenuItem miSalir;
    private org.edisoncor.gui.panel.PanelImage panelAvatar;
    private javax.swing.JPanel panelCentro;
    private javax.swing.JPanel panelDatos;
    private javax.swing.JPanel panelOtros;
    private javax.swing.JPanel panelUsuario;
    private javax.swing.JLabel textoMensaje;
    private javax.swing.JTextField textoMensajePersonal;
    private javax.swing.JTextField textoNombre;
    // End of variables declaration//GEN-END:variables
     
    private HiloCliente hiloCliente;
    private Socket socket;
    private Timer tiempoMensajeBarra;
    //
    private DefaultListModel <Usuario> modeloLista;
    private HashMap <Integer, Usuario> listaUsuarios;
    //
    private PanelConexion panelConexion;
    //
    private LayoutManager layoutAux;
    //
    private VentanaChat ventanaChat;
    //
    private JMenuItem []miEstados;
    //
    private static String RUTA = "/recursos/estado/";
}
