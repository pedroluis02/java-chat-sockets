/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import chat.VentanaChat;
import datos.Mensaje;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.jvnet.substance.SubstanceLookAndFeel;

/**
 *
 * @author Pedro Luis
 */

public class PrincipalCliente 
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
      //<editor-fold defaultstate="collapsed" desc="Nimbus Theme">  
      /*  
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        */
        //</editor-fold>
        
        // TODO code application logic here
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.BusinessBlackSteelSkin");
        SubstanceLookAndFeel.setCurrentWatermark("org.jvnet.substance.watermark.SubstanceStripeWatermark");
        //
        VentanaCliente vc = new VentanaCliente();
        vc.setTitle("Cliente Chat");
        vc.setLocationRelativeTo(null);
        vc.setVisible(true);
        //
//        final TrayIcon ti = new TrayIcon(
//                VentanaChat.obtenerRecursoIcono("chat.png").getImage(),
//                "Java Chat"
//                );
//        ti.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent ae) {
//                ti.displayMessage("Hola", "Nuevo Mensaje", TrayIcon.MessageType.ERROR);
//            }
//        });
//        
//        
//        final JPopupMenu jpopup = new JPopupMenu();
//
//        Image im = VentanaChat.obtenerRecursoIconoEstado(Mensaje.Estado.DISPONIBLE).getImage();
//        ImageIcon ni = new ImageIcon(im.getScaledInstance(16, 16, Image.SCALE_SMOOTH));
//        
//        JMenuItem mi = new JMenuItem("Example 1", 
////                VentanaChat.obtenerRecursoIconoEstado(Mensaje.Estado.DISPONIBLE),
//                ni
//                );
//        jpopup.add(mi);
//        
//        JMenuItem m2 = new JMenuItem("Example 2", 
//                VentanaChat.obtenerRecursoIconoEstado(Mensaje.Estado.AUSENTE));
//        jpopup.add(m2);
//
//        jpopup.addSeparator();
//
//        JMenuItem exitMI = new JMenuItem("Exit");
//        jpopup.add(exitMI);
//        
//        final Dimension d = SystemTray.getSystemTray().getTrayIconSize();
//        
//        System.out.println(d);
//        
//        ti.addMouseListener(new MouseAdapter() {
//        @Override
//        public void mouseReleased(MouseEvent e) {
//            if (e.getButton() == MouseEvent.BUTTON3) {
//                int ancho = (int)d.getWidth();
//                int alto = (int)d.getHeight();
//                int nuevoX = Math.abs(e.getX() - ancho);
//                int nuevoY = Math.abs(e.getY() - alto) + e.getY();
//                jpopup.setLocation(nuevoX, nuevoY);
//                jpopup.setInvoker(jpopup);
//                jpopup.setVisible(true);
//            }
//        }
//    });
//        
//        try {
//            if(SystemTray.isSupported()) {
//                SystemTray.getSystemTray().add(ti);
//            }
//        } catch (AWTException ex) {
//            System.err.println(ex.getMessage());
//        }
    }
}
