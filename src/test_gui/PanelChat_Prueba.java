/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test_gui;

import chat.cuadromensajes.CuadroIzquierda;
import chat.cuadromensajes.CuadroDerecha;
import chat.despligue.AreaTextoLlamada;
import cliente.VentanaCliente;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;

/**
 *
 * @author pedroluis
 */
public class PanelChat_Prueba extends javax.swing.JPanel {

    public PanelChat_Prueba() {
        initComponents();
        //
        areaMensajeEnvio.setMargin(new Insets(10, 10, 10, 10));
        //
        AreaTextoLlamada al1 = new AreaTextoLlamada(); al1.setOpaque(false);
        al1.insertarEnlace("(1): Pedro Luis"); al1.insertaNuevaLinea();
        al1.insertarTexto("hola como estas"); al1.insertaNuevaLinea();
        al1.insertarTexto("un gra dia hoy no crees...");
        CuadroIzquierda ci = new CuadroIzquierda(al1); ci.setOpaque(false);
        panelMensajeRecibidos.add(ci);
        
        AreaTextoLlamada al2 = new AreaTextoLlamada(AreaTextoLlamada.Orientacion.RIGHT);
        al2.setOpaque(false);
        al2.insertarEnlace("(1): Pedro Luis"); al2.insertaNuevaLinea();
        al2.insertarTexto("hola como estas"); al2.insertaNuevaLinea();
        al2.insertarTexto("un gra dia hoy no crees...");
        CuadroDerecha cd = new CuadroDerecha(al2); cd.setOpaque(false);
        panelMensajeRecibidos.add(cd);    
    }

    public static void main(String[] args) {
        // TODO code application logic here
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        JFrame f = new JFrame();
        f.add(new PanelChat_Prueba());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();
        f.setVisible(true);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        tabbedPaneClose1 = new org.edisoncor.gui.tabbedPane.TabbedPaneClose();
        jScrollPane3 = new javax.swing.JScrollPane();
        panelMensajeRecibidos = new org.edisoncor.gui.panel.Panel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        areaMensajeEnvio = new javax.swing.JTextArea();

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(1.0);

        jPanel1.setLayout(new java.awt.BorderLayout());

        tabbedPaneClose1.setModelo(org.edisoncor.gui.tabbedPane.TabbedPaneClose.Modelo.ROUND);

        panelMensajeRecibidos.setColorPrimario(new java.awt.Color(255, 255, 255));
        panelMensajeRecibidos.setColorSecundario(new java.awt.Color(255, 255, 255));
        panelMensajeRecibidos.setLayout(new javax.swing.BoxLayout(panelMensajeRecibidos, javax.swing.BoxLayout.PAGE_AXIS));
        jScrollPane3.setViewportView(panelMensajeRecibidos);

        tabbedPaneClose1.addTab("General", jScrollPane3);

        jPanel1.add(tabbedPaneClose1, java.awt.BorderLayout.CENTER);

        jSplitPane1.setLeftComponent(jPanel1);

        jPanel2.setLayout(new java.awt.BorderLayout());

        areaMensajeEnvio.setColumns(20);
        areaMensajeEnvio.setRows(5);
        areaMensajeEnvio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                areaMensajeEnvioKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(areaMensajeEnvio);

        jPanel2.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jSplitPane1.setRightComponent(jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void areaMensajeEnvioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_areaMensajeEnvioKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String mensaje = areaMensajeEnvio.getText();
            
            String []m = mensaje.split("\n");
            int i = 0;
            while(i < m.length) {
                if(!m[i].isEmpty()) {
                    System.out.println("'" + m[i] + "'");
                }
                i++;
            }
            System.out.println("---------------------------------------------------------------");
            areaMensajeEnvio.setText("");
        }
    }//GEN-LAST:event_areaMensajeEnvioKeyPressed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaMensajeEnvio;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSplitPane jSplitPane1;
    private org.edisoncor.gui.panel.Panel panelMensajeRecibidos;
    private org.edisoncor.gui.tabbedPane.TabbedPaneClose tabbedPaneClose1;
    // End of variables declaration//GEN-END:variables
}
