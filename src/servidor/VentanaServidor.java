
package servidor;

import datos.Usuario;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;

public class VentanaServidor extends javax.swing.JFrame {

    public VentanaServidor() {
        initComponents();
        try {
            HOST = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            HOST = "localhost";
        }
        
        textoIP.setText(HOST);
        textoPuerto.setText(PUERTO + "");
//        escuhando = true;
        
        listaUsuarios = new HashMap<>();
        contadorUsuarios = 0;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        textoIP = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textoPuerto = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        areaTextoConsola = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        botonLimpiar = new javax.swing.JButton();
        botonSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de Servidor"));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel1.setText("Dirección IP");

        textoIP.setEditable(false);

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel2.setText("Puerto");

        textoPuerto.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textoIP, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                    .addComponent(textoPuerto))
                .addGap(55, 55, 55))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textoIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(textoPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Consola"));
        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.LINE_AXIS));

        areaTextoConsola.setEditable(false);
        areaTextoConsola.setColumns(20);
        areaTextoConsola.setRows(5);
        areaTextoConsola.setMargin(new java.awt.Insets(10, 10, 10, 10));
        jScrollPane1.setViewportView(areaTextoConsola);

        jPanel2.add(jScrollPane1);

        jPanel3.setLayout(new java.awt.FlowLayout(2));

        botonLimpiar.setText("Limpiar");
        botonLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLimpiarActionPerformed(evt);
            }
        });
        jPanel3.add(botonLimpiar);

        botonSalir.setText("Salir de Servidor");
        botonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSalirActionPerformed(evt);
            }
        });
        jPanel3.add(botonSalir);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSalirActionPerformed
        for(int k: listaUsuarios.keySet()) {
            try {
                //listaUsuarios.get(k).stop();
                listaUsuarios.get(k).interrupt();
                listaUsuarios.get(k).socket.close();
            } catch (IOException ex) {
                mensajeConsola(ex.getMessage());
            }
        }
        System.exit(0);
    }//GEN-LAST:event_botonSalirActionPerformed

    private void botonLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonLimpiarActionPerformed
        // TODO add your handling code here:
        areaTextoConsola.setText("");
        mensajeConsola(new Date().toString()+": servidor iniciado");
    }//GEN-LAST:event_botonLimpiarActionPerformed

    // metodos
    public void iniciarServidor() {
        try {
            serverSocket = new ServerSocket(PUERTO);
            mensajeConsola(new Date().toString()+": servidor iniciado");
            //
            //
            while(true) {
                Socket socket = serverSocket.accept();
                HiloServidor hc = new HiloServidor(socket, this);
                
                int id = contadorUsuarios;
                listaUsuarios.put(id, hc);
                hc.start();
                contadorUsuarios++;
                
                hc.enviarRegistroId(id);
            }
        } catch (IOException ex) {
            mensajeConsola(ex.getMessage());
        }
    }
    public void mensajeConsola(String mensaje) {
        areaTextoConsola.append(mensaje + "\n");
        System.out.println(mensaje);
    }
    //
    public void nuevoUsuarioConectado(Usuario usuario) {
        mensajeConsola("Conectado: id=" + usuario.id);
        for(int k : listaUsuarios.keySet()) {
            listaUsuarios.get(k).enviarUsuarioConectado(usuario);
        }       
    }
    public void usuarioDesconectado(int id) {
        listaUsuarios.remove(id);
        mensajeConsola("Desconectado: id=" + id);
        for(int k : listaUsuarios.keySet()) {
            listaUsuarios.get(k).enviarUsuarioDesconectado(id);
        }       
    }
    public void cambioNombreUsuario(Usuario u) {
        mensajeConsola("Actualizo nombre: id=" + u.id);
        for(int k : listaUsuarios.keySet()) {
            listaUsuarios.get(k).enviarCambioNombreUsuario(u);
        } 
    }
    public void cambioMensajePersonalUsuario(Usuario u) {
        mensajeConsola("Actualizo Mensaje Personal: id=" + u.id);
        for(int k : listaUsuarios.keySet()) {
            listaUsuarios.get(k).enviarCambioMensajePersonalUsuario(u);
        } 
    }
    public void cambioEstadoUsuario(Usuario u) {
        mensajeConsola("Actualizo estado: id=" + u.id);
        for(int k : listaUsuarios.keySet()) {
            listaUsuarios.get(k).enviarCambioEstadoUsuario(u);
        } 
    }
    ///
    public void mensajeGeneral(int id, String mensaje) {
        mensajeConsola("Mensaje General: id=" + id + " mensaje=" + mensaje);
        for(int k : listaUsuarios.keySet()) {
            listaUsuarios.get(k).enviarMensajeGeneral(id, mensaje);
        }
    }
    public void mensajePrivado(int id, int idContacto, String mensaje) {
        mensajeConsola("Mensaje Privado: id=" + id + " mensaje=" + mensaje);
        listaUsuarios.get(id).enviarMensajePrivado(id, idContacto, mensaje);
        listaUsuarios.get(idContacto).enviarMensajePrivado(id, idContacto, mensaje);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaTextoConsola;
    private javax.swing.JButton botonLimpiar;
    private javax.swing.JButton botonSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField textoIP;
    private javax.swing.JTextField textoPuerto;
    // End of variables declaration//GEN-END:variables

    //mis variables
    public static HashMap <Integer, HiloServidor> listaUsuarios;
    private int contadorUsuarios;
    //
    private ServerSocket serverSocket;
    private int PUERTO = 5000;
    private boolean escuchando;
    private String HOST;
}
