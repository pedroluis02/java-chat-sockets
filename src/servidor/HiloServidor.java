/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

/**
 *
 * @author pedroluis
 */

import datos.Usuario;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import datos.Mensaje;

public class HiloServidor extends Thread {
    public Socket socket;
    private DataInputStream flujoLectura;
    private DataOutputStream flujoEscritura;
    public Usuario usuario;
    
    private VentanaServidor ventanaServidor;

    public HiloServidor(Socket socket, VentanaServidor ventanaServidor) {
        this.socket = socket;
        this.ventanaServidor = ventanaServidor;
        this.usuario = new Usuario();
        try {
            this.flujoLectura = new DataInputStream(socket.getInputStream());
            this.flujoEscritura = new DataOutputStream(socket.getOutputStream());
        } catch(IOException ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        }
    }
    @Override
    public void run() {
        int opcion;
       try {
           while(true) {   
               opcion = flujoLectura.readInt();
               switch(opcion) {
                   case Mensaje.DATOS:
                       //
                       usuario.nombre = flujoLectura.readUTF();
                       usuario.mensajePersonal = flujoLectura.readUTF();
                       usuario.estado = flujoLectura.readInt();
                       //
                       ventanaServidor.nuevoUsuarioConectado(usuario);
                       enviarListaUsuarios();
                       break;
                   case Mensaje.CAMBIO_NOMBRE:
                       //
                       usuario.nombre = flujoLectura.readUTF();
                       //
                       ventanaServidor.cambioNombreUsuario(usuario);
                       break;
                   case Mensaje.CAMBIO_MENSAJE_PERSONAL:
                       //
                       usuario.mensajePersonal = flujoLectura.readUTF();
                       //
                       ventanaServidor.cambioMensajePersonalUsuario(usuario);
                       break;
                   case Mensaje.CAMBIO_ESTADO:
                       //
                       usuario.estado = flujoLectura.readInt();
                       //
                       ventanaServidor.cambioEstadoUsuario(usuario);
                       break;
                   case Mensaje.MENSAJE_GNERAL:
                       String mg = flujoLectura.readUTF();
                       ventanaServidor.mensajeGeneral(usuario.id, mg);
                       break;
                   case Mensaje.MENSAJE_PRIVADO: 
                       int idContacto = flujoLectura.readInt();
                       String mgp = flujoLectura.readUTF();
                       ventanaServidor.mensajePrivado(usuario.id, idContacto, mgp);
                       break;
               }
           }
       } catch(Exception ex) {
           ventanaServidor.mensajeConsola(ex.getMessage());
       }       
       
       ventanaServidor.usuarioDesconectado(usuario.id);
    }
    public void enviarRegistroId(int id) {
        usuario.id = id;
        try {
            flujoEscritura.writeInt(Mensaje.REGISTRO);
            flujoEscritura.writeInt(id);
            ventanaServidor.mensajeConsola("Registro: id=" + id);
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        }
    }
    public void enviarListaUsuarios() {
        ventanaServidor.mensajeConsola(
                "Lista Usuarios: para=" + usuario.id + ": enviando...");
        try {
            flujoEscritura.writeInt(Mensaje.LISTA_USUARIOS);
            flujoEscritura.writeInt(VentanaServidor.listaUsuarios.size() - 1);
            Usuario u;
            for(int k : VentanaServidor.listaUsuarios.keySet()) {
                if( k != usuario.id) {
                    u = VentanaServidor.listaUsuarios.get(k).usuario;
                    flujoEscritura.writeInt(u.id);
                    flujoEscritura.writeUTF(u.nombre);
                    flujoEscritura.writeUTF(u.mensajePersonal);
                    flujoEscritura.writeInt(u.estado);
                }
            }
            ventanaServidor.mensajeConsola(
                    "Lista Usuarios: para=" + usuario.id + ": enviada");
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        }
    }
    public void enviarUsuarioConectado(Usuario usuario) {
        if(this.usuario.id == usuario.id) {
            return;
        }
        
        try {
            flujoEscritura.writeInt(Mensaje.USUARIO_CONECTADO);
            flujoEscritura.writeInt(usuario.id);
            flujoEscritura.writeUTF(usuario.nombre);
            flujoEscritura.writeUTF(usuario.mensajePersonal);
            flujoEscritura.writeInt(usuario.estado);
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        }
    }
    public void enviarUsuarioDesconectado(int id) {
        try {
            flujoEscritura.writeInt(Mensaje.USUARIO_DESCONECTADO);
            flujoEscritura.writeInt(id);
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        }
    }
    //
    public void enviarCambioNombreUsuario(Usuario usuario) {
        if(this.usuario.id == usuario.id) {
            return;
        }
        
        try {
            flujoEscritura.writeInt(Mensaje.CAMBIO_NOMBRE);
            flujoEscritura.writeInt(usuario.id);
            flujoEscritura.writeUTF(usuario.nombre);
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        }
    }
    public void enviarCambioMensajePersonalUsuario(Usuario usuario) {
        if(this.usuario.id == usuario.id) {
            return;
        }
        
        try {
            flujoEscritura.writeInt(Mensaje.CAMBIO_MENSAJE_PERSONAL);
            flujoEscritura.writeInt(usuario.id);
            flujoEscritura.writeUTF(usuario.mensajePersonal);
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        }
    }
    public void enviarCambioEstadoUsuario(Usuario usuario) {
        if(this.usuario.id == usuario.id) {
            return;
        }
        
        try {
            flujoEscritura.writeInt(Mensaje.CAMBIO_ESTADO);
            flujoEscritura.writeInt(usuario.id);
            flujoEscritura.writeInt(usuario.estado);
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        }
    }
    //
    public void enviarMensajeGeneral(int id, String mensaje) {
       try {
            flujoEscritura.writeInt(Mensaje.MENSAJE_GNERAL);
            flujoEscritura.writeInt(id);
            flujoEscritura.writeUTF(mensaje);
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        } 
    }
    public void enviarMensajePrivado(int id, int idContacto, String mensaje) {
       try {
            flujoEscritura.writeInt(Mensaje.MENSAJE_PRIVADO);
            flujoEscritura.writeInt(id);
            flujoEscritura.writeInt(idContacto);
            flujoEscritura.writeUTF(mensaje);
        } catch(Exception ex) {
            ventanaServidor.mensajeConsola(ex.getMessage());
        } 
    }
}