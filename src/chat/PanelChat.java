/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import chat.despligue.DespligueTextoMensaje;
import datos.Mensaje;
import datos.Usuario;
import javax.swing.Box;
import javax.swing.JScrollPane;

/**
 *
 * @author pedroluis
 */
public class PanelChat extends JScrollPane {
    private int idContacto;
    private Box panelMensajesRecibidos;
    private DespligueTextoMensaje dtm;
    private int estado;
    private boolean nuevoMensaje;
    public PanelChat(int idContacto, int estado, boolean general) {
        this.panelMensajesRecibidos = Box.createVerticalBox();
        this.idContacto = idContacto;
        setViewportView(panelMensajesRecibidos);
        //
        panelMensajesRecibidos.setOpaque(false);
        if(general) {
            dtm = new DespligueTextoMensaje.Despliegue_General(panelMensajesRecibidos);
        } else {
            dtm = new DespligueTextoMensaje.Despliegue_Privado(panelMensajesRecibidos);
        }
        //
        this.estado = estado;
        this.nuevoMensaje = false;
    }
    public void mostrarMensajeTextoGeneral(Usuario u, String mensaje) {
        dtm.elegirCuadroMensajes(idContacto, u, mensaje);
        //
    }
    
    public void mostrarMensajeTextoPrivado(Usuario u, String mensaje) {
        dtm.elegirCuadroMensajes(idContacto, u, mensaje);
        //
    }
    //
    public int getIdContacto() {
        return idContacto;
    }
    //
    public void setEstado(int estado) {
        this.estado = estado;
    }
    public int getEstado() {
        return estado;
    }
    public boolean isActivado() {
        return (estado != Mensaje.Estado.DESCONECTADO);
    }
    public void setNuevoMensaje(boolean nuevoMensaje) {
        this.nuevoMensaje = nuevoMensaje;
    }
    public boolean hasNuevoMensaje() {
        return nuevoMensaje;
    }
}
