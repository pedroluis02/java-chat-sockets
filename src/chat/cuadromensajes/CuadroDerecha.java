/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.cuadromensajes;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

/**
 *
 * @author pedroluis
 */
public class CuadroDerecha extends javax.swing.JPanel {

    public CuadroDerecha(JComponent areaMensajes) {
        initComponents();
        panel.add(areaMensajes);
        areaMensajes.updateUI();
    }
    
    public  JComponent componenteAreaMensajes() {
        return (JComponent)panel.getComponent(0);
    }
    
    public void setImagenAvatar(Image imagen) {
        Image scalada = imagen.getScaledInstance(32, 32, Image.SCALE_SMOOTH);
        buttonIcon_Avatar.setIcon(new ImageIcon(scalada));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        buttonIcon_Avatar = new org.edisoncor.gui.button.ButtonIcon();

        setOpaque(false);

        panel.setLayout(new java.awt.BorderLayout());

        buttonIcon_Avatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recursos/user.png"))); // NOI18N
        buttonIcon_Avatar.setText("buttonIcon1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonIcon_Avatar, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonIcon_Avatar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.button.ButtonIcon buttonIcon_Avatar;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
