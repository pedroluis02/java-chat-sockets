/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import cliente.VentanaCliente;
import datos.Mensaje;
import datos.Usuario;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 *
 * @author pedroluis
 */
public class VentanaChat extends javax.swing.JFrame {

    private int idUsuario;
    private static  String RECURSOS = "/recursos/chat/";
    public VentanaChat(VentanaCliente ventanaCliente, int idUsuario) {
        this.ventanaCliente = ventanaCliente;
        initComponents();
        //
        areaMensajeEnvio.setMargin(new Insets(10, 10, 10, 10));
        //
        addWindowStateListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });
        //
        panelesChat = new HashMap<>();
        //
        nuevoPanelChatGeneral(idUsuario);
        //
        this.idUsuario = idUsuario;
        //
//        nuevoPanelChatPrivado(new Usuario(1, "usuario 1", "", 0));
//        nuevoPanelChatPrivado(new Usuario(2, "usuario 2", "", 0));
//        panelesChat.get(1).setEstado(Mensaje.Estado.DESCONECTADO);
    }
    // nuevo panel chat
    public void nuevoPanelChatPrivado(Usuario contacto) {
        
        PanelChat pc = panelesChat.get(contacto.id);
        if(pc == null) {
            pc = new PanelChat(contacto.id, contacto.estado, false);
            panelesChat.put(contacto.id, pc);
            //
            tabsPanelesChat.addTab(null, pc);
            //
            TabBarClose tbcp = new TabBarClose(contacto.id,
                    obtenerRecursoIconoEstado(contacto.estado),
                    "(" + contacto.id + ")" + contacto.nombre);
            tbcp.addCloseTabActionListener(crearActionListenerButtonClose());
            //
            tabsPanelesChat.setTabComponentAt(tabsPanelesChat.getTabCount() - 1, tbcp);
        }
    }
    private void nuevoPanelChatGeneral(int idUsuario) {
        PanelChat pc = new PanelChat(idUsuario, Mensaje.Estado.DISPONIBLE, true);
        panelesChat.put(idUsuario, pc);
        //
        tabsPanelesChat.addTab("General", obtenerRecursoIcono("chat.png"), pc);
    }
    // nuevo mensaje
    public void mostraMensajeChatGeneral(Usuario u, int idUsuario, String mensaje) {
        panelesChat.get(idUsuario).mostrarMensajeTextoGeneral(u, mensaje);
    }
    
    public void mostraMensajeChatPrivado(Usuario u, int idContacto, String mensaje) {
        panelesChat.get(idContacto).mostrarMensajeTextoPrivado(u, mensaje);
    }
    //
    public void ponerFocoNuevoPanelChat(int idContacto) {
        tabsPanelesChat.setSelectedComponent(panelesChat.get(idContacto));
    }
    //
    public void cambioTabTitulo(int idContacto, String nuevoNombre) {
        PanelChat pc = panelesChat.get(idContacto);
        if(pc != null) {
            int index = tabsPanelesChat.indexOfComponent(pc);
            if(index != -1) {
                TabBarClose tbc = (TabBarClose)tabsPanelesChat.getTabComponentAt(index);
                tbc.setTabBarTitulo("(" + idContacto + ")" + nuevoNombre);
            }
        }
    }
    public void cambioTabIcono(int idContacto, int nuevoEstado) {
        PanelChat pc = panelesChat.get(idContacto);
        if(pc != null) {
            int index = tabsPanelesChat.indexOfComponent(pc);
            if(index != -1) {
                TabBarClose tbc = (TabBarClose)tabsPanelesChat.getTabComponentAt(index);
                pc.setEstado(nuevoEstado);
                if(!pc.hasNuevoMensaje()) {
                    tbc.setTabBarIcono(obtenerRecursoIconoEstado(nuevoEstado));
                }
            }
        }
    }
    //
    public void notificacionTabBarIcono(int idContacto) {
        PanelChat pc = panelesChat.get(idContacto);
        if(pc != null) {
            int index = tabsPanelesChat.indexOfComponent(pc);
            if(index != -1 && !pc.requestFocusInWindow()) {
                TabBarClose tbc = (TabBarClose)tabsPanelesChat.getTabComponentAt(index);
                tbc.setTabBarIcono(obtenerRecursoIcono("new-message.gif"));
                pc.setNuevoMensaje(true);
            }
        }
    }
    // desconexion
    public void desconexionContacto(int idContacto) {
        PanelChat pc = panelesChat.get(idContacto);
        if(pc != null) {
            pc.setEstado(Mensaje.Estado.DESCONECTADO);
            int index = tabsPanelesChat.indexOfComponent(pc);
            if(index != -1) {
                TabBarClose tbc = (TabBarClose)tabsPanelesChat.getTabComponentAt(index);
                pc.setEstado(Mensaje.Estado.DESCONECTADO);
                if(!pc.hasNuevoMensaje()) {
                    tbc.setTabBarIcono(
                        obtenerRecursoIconoEstado(Mensaje.Estado.DESCONECTADO));
                }
            }
            // notificacion chat desconexion
            // -----------------------------
        }
    }
    //
    public void cambioChatDesconexion() {
        //
    }
    /// remover tab
    private ActionListener crearActionListenerButtonClose() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                ButtonClose bc = (ButtonClose)ae.getSource();
                int id = bc.getIdButton();
                //
                removerTabPanelChat(id);
            }
        };
    }
    private void removerTabPanelChat(int id) {
        PanelChat pc = panelesChat.get(id);
        if(pc != null) {
            tabsPanelesChat.remove(pc);
            panelesChat.remove(id);
        }
    }
    // recursos static methods
    public static ImageIcon obtenerRecursoIcono(String nombre) {
        return new ImageIcon(
                VentanaChat.class.getResource(RECURSOS + nombre));
    }
    public static ImageIcon obtenerRecursoIconoEstado(int estado) {
        return VentanaCliente.obtenerRecursoIconoEstado(estado);
    }
    //
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        tabsPanelesChat = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        areaMensajeEnvio = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Chat");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
            }
        });

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(1.0);

        jPanel2.setLayout(new java.awt.BorderLayout());

        tabsPanelesChat.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tabsPanelesChatStateChanged(evt);
            }
        });
        jPanel2.add(tabsPanelesChat, java.awt.BorderLayout.CENTER);

        jSplitPane1.setLeftComponent(jPanel2);

        jPanel3.setLayout(new java.awt.BorderLayout());

        areaMensajeEnvio.setColumns(20);
        areaMensajeEnvio.setRows(5);
        areaMensajeEnvio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                areaMensajeEnvioKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(areaMensajeEnvio);

        jPanel3.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jSplitPane1.setRightComponent(jPanel3);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void areaMensajeEnvioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_areaMensajeEnvioKeyPressed
        // TODO add your handling code here:
        if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            String mensaje = areaMensajeEnvio.getText();
            //
            PanelChat ps = (PanelChat)tabsPanelesChat.getSelectedComponent();
            if(ps.getIdContacto() == idUsuario) {
                ventanaCliente.enviarMensajeHiloGeneral(mensaje);
            } else {
                ventanaCliente.enviarMensajeHiloPrivado(ps.getIdContacto(), mensaje);
            }
            //
            areaMensajeEnvio.setText("");
        }
    }//GEN-LAST:event_areaMensajeEnvioKeyPressed

    private void tabsPanelesChatStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tabsPanelesChatStateChanged
        // TODO add your handling code here:
        PanelChat pc = (PanelChat)tabsPanelesChat.getSelectedComponent();
        areaMensajeEnvio.setEditable(pc.isActivado());
        if(pc.getIdContacto() != idUsuario) {
            if(pc.hasNuevoMensaje()) {
                int index = tabsPanelesChat.getSelectedIndex();
                TabBarClose tbc = (TabBarClose)tabsPanelesChat.getTabComponentAt(index);
                tbc.setTabBarIcono(
                        obtenerRecursoIconoEstado(pc.getEstado()));
                pc.setNuevoMensaje(false);
            }
        }
    }//GEN-LAST:event_tabsPanelesChatStateChanged

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        // TODO add your handling code here:
        PanelChat pc = (PanelChat)tabsPanelesChat.getSelectedComponent();
        if(pc != null) {
            if(idUsuario != pc.getIdContacto() && pc.hasNuevoMensaje()) {
                int index = tabsPanelesChat.getSelectedIndex();
                TabBarClose tbc = (TabBarClose)tabsPanelesChat.getTabComponentAt(index);
                tbc.setTabBarIcono(
                        obtenerRecursoIconoEstado(pc.getEstado()));
                pc.setNuevoMensaje(false);
                
                areaMensajeEnvio.setEditable(pc.isActivado());
            }
        }
    }//GEN-LAST:event_formWindowGainedFocus

    public static void main(String args[]) {
        VentanaChat vc = new VentanaChat(null, 0);
        vc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vc.setVisible(true);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaMensajeEnvio;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane tabsPanelesChat;
    // End of variables declaration//GEN-END:variables
    private VentanaCliente ventanaCliente;
//    private boolean control;
    private HashMap <Integer, PanelChat> panelesChat;
}
