/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.FontUIResource;

/**
 *
 * @author pedroluis
 */
public class TabBarClose extends JPanel {
    private JLabel jlIcono;
    private JLabel jlTitulo;
    private JButton jbCerrar;
    private FlowLayout fLayout;
    
    public TabBarClose(int id, Icon icono, String titulo) {
        fLayout = new FlowLayout();
        fLayout.setVgap(2);
        setLayout(fLayout);
        jlIcono = new JLabel(icono, JLabel.CENTER);
        jlTitulo = new JLabel(titulo);
        setFont(new FontUIResource("Dialog", Font.BOLD, 10));
        jlTitulo.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jbCerrar = new ButtonClose(id);
        
        add(jlIcono); add(jlTitulo); add(jbCerrar);
        
        setOpaque(false);
    }
    
    public TabBarClose(int id, String titulo) {
        this(id, null, titulo);
    }
    
    public void addCloseTabActionListener(ActionListener al) {
        jbCerrar.addActionListener(al);
    }
    
    public void setTabBarIcono(Icon icono) {
        jlIcono.setIcon(icono);
    }
    
    public void setTabBarTitulo(String titulo) {
        jlTitulo.setText(titulo);
    }

    public String getTabBarTitulo() {
        return jlTitulo.getText();
    }
    
    public Icon getTabBarIcono() {
        return jlIcono.getIcon();
    }
}
