
package chat.despligue;

/**
 *
 * @author pedroluis
 */
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.event.MouseInputAdapter;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author pedroluis
 */
public class AreaTextoLlamada extends JTextPane {

    protected Color colorDeBorde = new Color(177, 177, 177);

    public enum Orientacion{
        LEFT,
        RIGHT
    }
    protected Orientacion orientacion= Orientacion.LEFT;
    protected int distancia = 15;
    protected int ancho = 10;
    protected float anchoDeBorde=1.5f;
    
    public AreaTextoLlamada(Orientacion orientacion) {
        setOpaque(true);
        setMargin(new Insets(0, 35, 0, 0));
        setEditable(false);
        this.orientacion = orientacion;
    }

    public AreaTextoLlamada() {
        this(Orientacion.LEFT);
    }
    
    @Override
     protected void paintBorder(Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setStroke(new BasicStroke(anchoDeBorde));
        g2.setColor(colorDeBorde);
        g2.draw(getShape());
        g2.dispose();
    }

   public Shape getShape(){
        switch (orientacion){
            case LEFT : {
                Shape shape = new RoundRectangle2D.Float(distancia, 0,
                        getWidth()-distancia-1, getHeight()-1,
                        getHeight()/3, getHeight()/3);
                Area area = new Area(shape);
                GeneralPath path = new GeneralPath();
                path.moveTo(0, getHeight()/4);
                path.lineTo(distancia, getHeight()/4);
                path.lineTo(distancia, (getHeight()/2)+ancho);
                path.closePath();
                area.add(new Area(path));
                return area;
            }
            default: {
                Shape shape = new RoundRectangle2D.Float(0, 0,
                        getWidth()-distancia, getHeight()-1,
                        getHeight()/3, getHeight()/3);
                Area area = new Area(shape);
                GeneralPath path = new GeneralPath();
                path.moveTo(getWidth(), getHeight()/4);
                path.lineTo(getWidth()-distancia, (getHeight()/4));
                path.lineTo(getWidth()-distancia, (getHeight()/2)+ancho);
                path.closePath();
                area.add(new Area(path));
                return area;
            }
        }
        
    }
   //
   public void insertaNuevaLinea() {
      // Atributos null
        try {
            getStyledDocument()
              .insertString(
            getStyledDocument().getLength(),
            System.getProperty("line.separator"), null);
        } catch(BadLocationException ble) {
            
        }
    }
    
    public void insertarEnlace(String titulo) {
        final JLabel l = new JLabel("<html><u><b>" + titulo + "</b></u></hmtl>");
        l.setToolTipText(titulo);
        l.setBackground(Color.red);
        l.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                l.setText("<html><b><a href='#'>" + l.getToolTipText() + "</a></b></hmtl>");
            }
            @Override
            public void mouseExited(MouseEvent e) {
                l.setText("<html><u><b>" + l.getToolTipText() + "</b></u></hmtl>");
            }
            @Override
            public void mouseClicked(MouseEvent e) {
                JOptionPane.showMessageDialog(null, l.getToolTipText());
            }
        });
        insertComponent(l);
    }
    
    public void insertarTitulo(String titulo) {
        try {
            SimpleAttributeSet attrs = new SimpleAttributeSet();
            StyleConstants.setRightIndent(attrs, 10);
            StyleConstants.setBold(attrs, true);
            StyleConstants.setUnderline(attrs, true);
            getStyledDocument()
              .insertString(
            getStyledDocument().getLength(), titulo, attrs);
        } catch(BadLocationException ble) {
            
        }
    }
    
    public void insertarTexto(String titulo) {
        try {
            SimpleAttributeSet attrs = new SimpleAttributeSet();
            getStyledDocument()
                .insertString(
            getStyledDocument().getLength(), titulo, attrs);
        } catch(BadLocationException ble) {
            
        }
    }
   
   //

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public Color getColorDeBorde() {
        return colorDeBorde;
    }

    public void setColorDeBorde(Color colorDeBorde) {
        this.colorDeBorde = colorDeBorde;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public Orientacion getOrientacion() {
        return orientacion;
    }

    public void setOrientacion(Orientacion orientacion) {
        this.orientacion = orientacion;
    }

    public float getAnchoDeBorde() {
        return anchoDeBorde;
    }

    public void setAnchoDeBorde(float anchoDeBorde) {
        this.anchoDeBorde = anchoDeBorde;
    }

}