/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.despligue;

import chat.LineaTextoMensaje;
import datos.Usuario;
import java.awt.Color;
import javax.swing.Box;
import chat.cuadromensajes.CuadroDerecha;
import chat.cuadromensajes.CuadroIzquierda;

/**
 *
 * @author pedroluis
 */
public class DespligueTextoMensaje { 
    protected Box panelMensajesRecibidos;
    protected boolean control;
    protected int idUsuarioUltimo = -1;
    public DespligueTextoMensaje(Box panelMensajeRecidos) {
        this.panelMensajesRecibidos = panelMensajeRecidos;
        this.control = true;
        //
        panelMensajeRecidos.setOpaque(true);
        panelMensajeRecidos.setBackground(Color.WHITE);
    }
    public boolean getControl() {
        return control;
    }
    public void insertarTitulo(Usuario u) {}
    public void insertarLineaTexto(String lineaTexto) {}
    public void elegirCuadroMensajes(int idContacto, Usuario u, String mensaje) {}
    public void desplegarCuadroMensajes(Usuario u, String mensaje) {
        String[] filas = mensaje.split("\n");
        int i = 0;
        while(i < filas.length) {
            if(!filas[i].isEmpty()) {
                insertarLineaTexto(filas[i]);
            }
            i++;
        }
    }
    public void desplegarCuadroIzq(Usuario u, String mensaje) {}
    public void desplegarCuadroDer(Usuario u, String mensaje) {}
    
    // -----------------------------------------------------------------
    public static class Despliegue_General extends DespligueTextoMensaje {
        private AreaTextoLlamada areaLlamada;
        public Despliegue_General(Box panelMensajeRecidos) {
            super(panelMensajeRecidos);
        }
        @Override
        public void insertarTitulo(Usuario u) {
            areaLlamada.insertarTitulo("(" + u.id + "): " + u.nombre);
            areaLlamada.insertaNuevaLinea();
        }
        @Override
        public void insertarLineaTexto(String lineaTexto) {
            areaLlamada.insertarTexto(lineaTexto);
            areaLlamada.insertaNuevaLinea();
        }
        @Override
        public void elegirCuadroMensajes(int idContacto, Usuario u, String mensaje) {
            if(idUsuarioUltimo == u.id) {
                if(control) {
                    desplegarCuadroIzq(u, mensaje);
                } else {
                    desplegarCuadroDer(u, mensaje);
                }
            } else {
                //
                control = !control;
                //
                if(control) {
                    desplegarCuadroIzq(u, mensaje);
                } else {
                    desplegarCuadroDer(u, mensaje);
                }
            }
        }
        @Override
        public void desplegarCuadroIzq(Usuario u, String mensaje) {
            if(idUsuarioUltimo == u.id) {
                CuadroIzquierda ci = (CuadroIzquierda)panelMensajesRecibidos.getComponent(
                        panelMensajesRecibidos.getComponentCount() - 1);
                areaLlamada = (AreaTextoLlamada)ci.componenteAreaMensajes();
                //
                desplegarCuadroMensajes(u, mensaje);
                //
                areaLlamada.updateUI();
            } else {
                //
                idUsuarioUltimo = u.id;
                //
                areaLlamada = new AreaTextoLlamada();
                //
                insertarTitulo(u);
                desplegarCuadroMensajes(u, mensaje);
                //
                panelMensajesRecibidos.add(new CuadroIzquierda(areaLlamada));
            }
        }
        @Override
        public void desplegarCuadroDer(Usuario u, String mensaje) {
            if(idUsuarioUltimo == u.id) {
                CuadroDerecha cd = (CuadroDerecha)panelMensajesRecibidos.getComponent(
                        panelMensajesRecibidos.getComponentCount() - 1);
                areaLlamada = (AreaTextoLlamada)cd.componenteAreaMensajes();
                //
                desplegarCuadroMensajes(u, mensaje);
                //
                areaLlamada.updateUI();
            } else {
                //
                idUsuarioUltimo = u.id;
                //
                areaLlamada = new AreaTextoLlamada(AreaTextoLlamada.Orientacion.RIGHT);   
                //
                insertarTitulo(u);
                desplegarCuadroMensajes(u, mensaje);
                //
                panelMensajesRecibidos.add(new CuadroDerecha(areaLlamada));
            }
        }
    }
    
    //
    public static class Despliegue_Privado extends DespligueTextoMensaje {
        private PanelTextoLlamada panelTextoLlamada;
        public Despliegue_Privado(Box panelMensajeRecidos) {
            super(panelMensajeRecidos);
        }
        @Override
        public void insertarTitulo(Usuario u) {
            panelTextoLlamada.add(new LineaTextoMensaje(
                    "<html><u><b>"
                    + "(" + u.id + "): " + u.nombre
                    + "</b></u></html>"));
        }
        @Override
        public void insertarLineaTexto(String lineaTexto) {
            panelTextoLlamada.add(new LineaTextoMensaje(lineaTexto));
        }
        @Override
        public void desplegarCuadroIzq(Usuario u, String mensaje) {
            if(idUsuarioUltimo == u.id) {
                CuadroIzquierda ci = (CuadroIzquierda)panelMensajesRecibidos.getComponent(
                        panelMensajesRecibidos.getComponentCount() - 1);
                panelTextoLlamada = (PanelTextoLlamada)ci.componenteAreaMensajes();
                //
                desplegarCuadroMensajes(u, mensaje);
                //
                panelTextoLlamada.updateUI();
            } else {
                //
                idUsuarioUltimo = u.id;
                //
                panelTextoLlamada = new PanelTextoLlamada();
                panelTextoLlamada.setColorPrimario(new Color(51,102,255));
                panelTextoLlamada.setColorSecundario(new Color(204,204,255));
                //
                insertarTitulo(u);
                desplegarCuadroMensajes(u, mensaje);
                //
                panelMensajesRecibidos.add(new CuadroIzquierda(panelTextoLlamada));
            }
        }
        @Override
        public void elegirCuadroMensajes(int idContacto, Usuario u, String mensaje) {
            if(idContacto != u.id) {
                desplegarCuadroIzq(u, mensaje);
            } else {
                desplegarCuadroDer(u, mensaje);
            }
        }
        @Override
        public void desplegarCuadroDer(Usuario u, String mensaje) {
            if(idUsuarioUltimo == u.id) {
                CuadroDerecha cd = (CuadroDerecha)panelMensajesRecibidos.getComponent(
                        panelMensajesRecibidos.getComponentCount() - 1
                        );
                panelTextoLlamada = (PanelTextoLlamada)cd.componenteAreaMensajes();
                //
                desplegarCuadroMensajes(u, mensaje);
                //
                panelTextoLlamada.updateUI();
            } else {
                //
                idUsuarioUltimo = u.id;
                //
                panelTextoLlamada = new PanelTextoLlamada(PanelTextoLlamada.Orientacion.RIGHT);
                panelTextoLlamada.setColorPrimario(new Color(255,255,102));
                panelTextoLlamada.setColorSecundario(new Color(255,255,153));
                //
                insertarTitulo(u);
                desplegarCuadroMensajes(u, mensaje);
                //
                panelMensajesRecibidos.add(new CuadroDerecha(panelTextoLlamada));
            }
        }
    }
}
