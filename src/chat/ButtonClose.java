/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.plaf.basic.BasicButtonUI;

/**
 *
 * @author pedroluis
 */

public class ButtonClose extends JButton {
    private int id;
    public ButtonClose(int id) {
        
        setPreferredSize(new Dimension(15, 15));
        setUI(new BasicButtonUI());
        setBorder(BorderFactory.createEtchedBorder());
        setBorderPainted(false);
        setFocusable(false);
        setContentAreaFilled(false);
        setRolloverEnabled(true);
        //
        this.id = id;
    }
    
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        Line2D l1 = new Line2D.Double(5, 5, 10, 10);
        Line2D l2 = new Line2D.Double(10, 5, 5, 10);
        Rectangle2D r2d = l1.getBounds2D();
        r2d.setFrame(r2d.getX() - 4, r2d.getY() - 4, r2d.getWidth() + 9,
                        r2d.getHeight() + 9);
        Ellipse2D e = new Ellipse2D.Double();
            e.setFrame(r2d);
        if(getModel().isRollover()) {
                g2.setStroke(new BasicStroke(1));
                g2.setColor(Color.RED);
                g2.fill(e);
                g2.setColor(Color.WHITE);
        }
        g2.setStroke(new BasicStroke(2));
        g2.draw(l1); g2.draw(l2);
        g2.dispose();
    }
    //
    public int getIdButton() {
        return id;
    }
}
