/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

/**
 *
 * @author pedroluis
 */
public class Usuario {
    public int id;
    public String nombre;
    public String mensajePersonal;
    public int estado;

    public Usuario() {
        this.id = -1;
        this.nombre = "";
        this.mensajePersonal = "";
        this.estado = Mensaje.Estado.DISPONIBLE;
    }

    public Usuario(int id, String nombre, String mensajePersonal, int estado) {
        this.id = id;
        this.nombre = nombre;
        this.mensajePersonal = mensajePersonal;
        this.estado = estado;
    }
}
