/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

/**
 *
 * @author pedroluis
 */
public class Mensaje {
    public static final int REGISTRO = 0;
    public static final int DATOS = 1;
    public static final int LISTA_USUARIOS = 2;
    public static final int USUARIO_CONECTADO = 3;
    public static final int USUARIO_DESCONECTADO = 4;
    //
    public static final int MENSAJE_GNERAL = 5;
    //
    public static final int CAMBIO_NOMBRE = 6;
    public static final int CAMBIO_MENSAJE_PERSONAL = 7;
    //
    public static final int MENSAJE_PRIVADO = 8;
    //
    public static final int CAMBIO_ESTADO = 9;
    
    public static class Estado {
        public static final int DISPONIBLE = 0;
        public static final int OCUPADO = 1;
        public static final int AUSENTE = 2;
        public static final int INACTIVO = 3;
        public static final int DESCONECTADO = 4;
    }
}
